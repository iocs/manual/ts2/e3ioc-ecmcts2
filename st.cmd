##############################################################################
## EtherCAT Motion Control TS2 IOC configuration

##############################################################################
## Initiation:
epicsEnvSet("IOC" ,"$(IOC="TS2:Ctrl-ECAT-001")")
epicsEnvSet("ECMCCFG_INIT" ,"")  #Only run startup once (auto at PSI, need call at ESS), variable set to "#" in startup.cmd
epicsEnvSet("SCRIPTEXEC" ,"$(SCRIPTEXEC="iocshLoad")")

epicsEnvSet("NAMING","ESSnaming")

require ecmccfg 7.0.1
require essioc

# Load standard module startup scripts
iocshLoad("$(essioc_DIR)/common_config.iocsh")

#- Choose motor record driver implementation
#-   ECMC_MR_MODULE="ecmcMotorRecord"  => ECMC native built in motor record support (Default)
#-   ECMC_MR_MODULE="EthercatMC"       => Motor record support from EthercatMC module (need to be loaded)
#- Uncomment the line below to use EthercatMC (and add optional EthercatMC_VER to startup.cmd call):
#- epicsEnvSet(ECMC_MR_MODULE,"EthercatMC")

# Epics Motor record driver that will be used:
# epicsEnvShow(ECMC_MR_MODULE)

# run module startup.cmd (only needed at ESS  PSI auto call at require)
$(ECMCCFG_INIT)$(SCRIPTEXEC) ${ecmccfg_DIR}startup.cmd, "IOC=$(IOC),ECMC_VER=7.0.1"

##############################################################################

# Configure hardware:
ecmcFileExist($(E3_CMD_TOP)/hw/ecmcMCUTS2.cmd,1)
$(SCRIPTEXEC) $(E3_CMD_TOP)/hw/ecmcMCUTS2.cmd

# Apply hardware configuration
ecmcConfigOrDie "Cfg.EcApplyConfig(1)"

# ADDITIONAL SETUP
# Set all outputs to feed switches
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(${ECMC_EC_SLAVE_NUM_DIG_OUT},binaryOutput01,1)"
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(${ECMC_EC_SLAVE_NUM_DIG_OUT},binaryOutput02,1)"
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(${ECMC_EC_SLAVE_NUM_DIG_OUT},binaryOutput03,1)"
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(${ECMC_EC_SLAVE_NUM_DIG_OUT},binaryOutput04,1)"
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(${ECMC_EC_SLAVE_NUM_DIG_OUT},binaryOutput05,1)"
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(${ECMC_EC_SLAVE_NUM_DIG_OUT},binaryOutput06,1)"
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(${ECMC_EC_SLAVE_NUM_DIG_OUT},binaryOutput07,1)"
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(${ECMC_EC_SLAVE_NUM_DIG_OUT},binaryOutput08,1)"
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(${ECMC_EC_SLAVE_NUM_DIG_OUT},binaryOutput09,1)"
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(${ECMC_EC_SLAVE_NUM_DIG_OUT},binaryOutput10,1)"
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(${ECMC_EC_SLAVE_NUM_DIG_OUT},binaryOutput11,1)"
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(${ECMC_EC_SLAVE_NUM_DIG_OUT},binaryOutput12,1)"
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(${ECMC_EC_SLAVE_NUM_DIG_OUT},binaryOutput13,1)"
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(${ECMC_EC_SLAVE_NUM_DIG_OUT},binaryOutput14,1)"
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(${ECMC_EC_SLAVE_NUM_DIG_OUT},binaryOutput15,1)"
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(${ECMC_EC_SLAVE_NUM_DIG_OUT},binaryOutput16,1)"
# END of ADDITIONAL SETUP


$(SCRIPTEXEC) $(ecmccfg_DIR)loadPLCFile.cmd, "PLC_ID=0, SAMPLE_RATE_MS=100,FILE=$(E3_CMD_TOP)/plc/autopoweroff.plc, PLC_MACROS='PLC_ID=0,DBG='")

##############################################################################
## AXIS 1
#
epicsEnvSet("DEV",      "TS2-010RFC:RFS-VPD-010")
$(SCRIPTEXEC) ($(ecmccfg_DIR)configureAxis.cmd, CONFIG=$(E3_CMD_TOP)/cfg/linear_1_PowerDivider.ax)

##############################################################################
## AXIS 2
#
epicsEnvSet("DEV",      "TS2-010RFC:RFS-VPD-020")
$(SCRIPTEXEC) ($(ecmccfg_DIR)configureAxis.cmd, CONFIG=$(E3_CMD_TOP)/cfg/linear_2_PowerDivider.ax)

##############################################################################
## AXIS 3
#
epicsEnvSet("DEV",      "TS2-010CRM:EMR-SM-010")
$(SCRIPTEXEC) ($(ecmccfg_DIR)configureAxis.cmd, CONFIG=$(E3_CMD_TOP)/cfg/linear_3_CavityTuner1.ax)

ecmcConfigOrDie "Cfg.SetAxisEnableMotionFunctions(3,1,0,1)"

##############################################################################
## AXIS 4
#
epicsEnvSet("DEV",      "TS2-010CRM:EMR-SM-020")
$(SCRIPTEXEC) ($(ecmccfg_DIR)configureAxis.cmd, CONFIG=$(E3_CMD_TOP)/cfg/linear_4_CavityTuner2.ax)

ecmcConfigOrDie "Cfg.SetAxisEnableMotionFunctions(4,1,0,1)"

##############################################################################
## AXIS 5
#
epicsEnvSet("DEV",      "TS2-010CRM:EMR-SM-030")
$(SCRIPTEXEC) ($(ecmccfg_DIR)configureAxis.cmd, CONFIG=$(E3_CMD_TOP)/cfg/linear_5_CavityTuner3.ax)

ecmcConfigOrDie "Cfg.SetAxisEnableMotionFunctions(5,1,0,1)"

##############################################################################
## AXIS 6
#
epicsEnvSet("DEV",      "TS2-010CRM:EMR-SM-040")
$(SCRIPTEXEC) ($(ecmccfg_DIR)configureAxis.cmd, CONFIG=$(E3_CMD_TOP)/cfg/linear_6_CavityTuner4.ax)

ecmcConfigOrDie "Cfg.SetAxisEnableMotionFunctions(6,1,0,1)"

##############################################################################
############# Configure diagnostics:

ecmcConfigOrDie "Cfg.EcSetDiagnostics(1)"
ecmcConfigOrDie "Cfg.EcEnablePrintouts(0)"
ecmcConfigOrDie "Cfg.EcSetDomainFailedCyclesLimit(100)"
ecmcConfigOrDie "Cfg.SetDiagAxisIndex(1)"
ecmcConfigOrDie "Cfg.SetDiagAxisFreq(2)"
ecmcConfigOrDie "Cfg.SetDiagAxisEnable(0)"

# go active
$(SCRIPTEXEC) ($(ecmccfg_DIR)setAppMode.cmd)

dbLoadRecords($(E3_CMD_TOP)/db/CVT_autosave.db,  "NAME=TS2-010CRM:EMR-SM-010:Motor")
dbLoadRecords($(E3_CMD_TOP)/db/CVT_autosave.db,  "NAME=TS2-010CRM:EMR-SM-020:Motor")
dbLoadRecords($(E3_CMD_TOP)/db/CVT_autosave.db,  "NAME=TS2-010CRM:EMR-SM-030:Motor")
dbLoadRecords($(E3_CMD_TOP)/db/CVT_autosave.db,  "NAME=TS2-010CRM:EMR-SM-040:Motor")


iocInit()

